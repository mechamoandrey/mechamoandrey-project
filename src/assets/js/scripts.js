var slider = tns({
  container: '.my-slider',
  items: 1,
  autoplay: true,
  "axis": "vertical",
  autoplayTimeout: 2500,
  nav: false,
  controls: false,
  autoplayButtonOutput: false,
  center: true,
  mouseDrag:false,
  touch: false
});

var slider = tns({
  container: '.skills__carousel',
  items: 1,
  nav: true,
  controls: false,
  mouseDrag: true,
  edgePadding: 0,
});

// let terminal = document.querySelector('.terminal');
// let simulationTextTerminal = terminal.querySelector('.terminal__input-user');
// let textTerminal = terminal.querySelector('.terminal__content-fixed--disable') ;


// let getPositionTerminal = function add() {

//   if(terminal.getBoundingClientRect().y <= window.screen.height/2){
//     window.removeEventListener('scroll', add, false);

//     simulationTextTerminal.classList.add("anim-typewriter");

//     setTimeout(function(){ 
//       simulationTextTerminal.classList.add("terminal__input-user--no-border")

//     }, 2400);
    
//   } 
// }

let skillsBlock = document.querySelector('.skills');

let getPositionSkill = function add() {

  if(skillsBlock.getBoundingClientRect().y <= window.screen.height/2){
    window.removeEventListener('scroll', add, false);
    skillsBlock.classList.add("active");
  } 
}

// window.addEventListener('scroll', getPositionTerminal);
window.addEventListener('scroll', getPositionSkill);
