let gulp = require('gulp');
let del = require('del');
let gulpsass = require('gulp-sass');
let cleanCSS = require('gulp-clean-css');
let autoprefixer = require('gulp-autoprefixer');
let sourcemaps = require('gulp-sourcemaps');
let rename = require('gulp-rename');
let uglify = require('gulp-uglify');
let image = require('gulp-image');
let newer = require('gulp-newer');
let htmlMin = require('gulp-html-minify');
let fs = require('fs');
let path = require('path');
let browserSync = require('browser-sync').create();

// Paths
const paths = {
    scss: {
        src: 'src/**/*.scss',
        dest: 'dist/'
    },
    htmls: {
        src: 'src/**/*.html',
        dest: 'dist/'
    },
    images: {
        src: 'src/**/*.+(jpg|jpeg|gif|png|svg)',
        dest: 'dist/'
    },
    scripts: {
        src: 'src/**/*.js',
        dest: 'dist/'
    },
    outros: {
        src: 'src/**/*.+(css|pdf|zip)',
        dest: 'dist/'
    },
};

function clean() {
    return del(['dist/**/*']);
}

function sass() {
    return gulp.src(paths.scss.src)
        .pipe(sourcemaps.init())
        .pipe(gulpsass())
        .pipe(cleanCSS({rebase: false}))
        .pipe(autoprefixer({
            browsers: ['last 2 versions']
        }))
        // .pipe(rename({ suffix: ".min"}))
        .pipe(sourcemaps.write("."))
        .pipe(gulp.dest(paths.scss.dest))
        .pipe(browserSync.stream());
}

function html() {
    return gulp.src(paths.htmls.src)
        .pipe(gulp.dest(paths.htmls.dest))
        .pipe(browserSync.stream());
}

function images() {
    return gulp.src(paths.images.src)
        .pipe(newer(paths.images.dest))
        .pipe(image({
            optipng: ['-i 1', '-strip all', '-fix', '-o7', '-force'],
            pngquant: false,
            zopflipng: false,
            jpegRecompress: ['--strip', '--quality', 'high'],
            mozjpeg: false,
            guetzli: false,
            gifsicle: false,
            svgo: false
        }))
        .pipe(gulp.dest(paths.images.dest))
        .pipe(browserSync.stream());
}

function script() {
    return gulp.src(paths.scripts.src)
        .pipe(sourcemaps.init())
        // .pipe(uglify())
        // .pipe(rename({ suffix: ".min"}))
        .pipe(sourcemaps.write("."))
        .pipe(gulp.dest(paths.scripts.dest))
        .pipe(browserSync.stream());
}


function outros() {
    return gulp.src(paths.outros.src)
        .pipe(gulp.dest(paths.outros.dest))
        .pipe(browserSync.stream());
}

function watch() {
    browserSync.init({
        server: "./dist",
        directory: true
    });

    gulp.watch(paths.scss.src, sass);
    gulp.watch(paths.htmls.src, html);
    gulp.watch(paths.images.src, images);
    gulp.watch(paths.scripts.src, script);
    gulp.watch(paths.outros.src, outros);
}

gulp.task("default", gulp.series(sass, html, images, script, outros, watch));

gulp.task("limpar", clean);
